
let shuffle = require('shuffle-array');

exports.getRandom = () => {
	let i = 1;
	let arr = [];
	for(i;i<=20;i++) {
		arr.push({"name": "image" + i, "url" : "static/image" + i + ".jpg"});
	}

	let random = Math.floor(Math.random() * 10);

	if(random > 5) {
		return {status : false};
	}else {
		let item = shuffle.pick(arr, { 'picks': Math.floor(Math.random() * 20) }); 
		return {status : true, data : item};
	}
	
}


exports.getNumberRandom = (number) => {
	let i = 1;
	let arr = [];
	for(i;i<=20;i++) {
		arr.push({"name": "image" + i, "url" : "static/image" + i + ".jpg"});
	}

	let random = Math.floor(Math.random() * 10);

	if(random > 5) {
		return {status : false};
	}else {
		let item = shuffle.pick(arr, { 'picks': parseInt(number) }); 
		if(item.length == undefined) {
			item = [item];
		}
		return {status : true, data : item};
	}
	
}

