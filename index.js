var app = require('express')();
let express = require('express');

var images = require('./images');


var port = process.env.PORT || 8000;

app.get('/api', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed
	let data = images.getRandom();
	if(data.status) {
		res.json(data);
	}else {
		res.statusCode = 401;
		res.send('Server is down');
	}
    
});

app.get('/api/:id', (req, res) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed
	
	if(req.params.id == 0) {
		let data = images.getRandom();
		if(data.status) {
			res.json(data);
		}else {
			res.statusCode = 401;
			res.send('Server is down');
		}
	}else {
		let data = images.getNumberRandom(req.params.id);
		if(data.status) {
			res.json(data);
		}else {
			res.statusCode = 401;
			res.send('Server is down');
		}
	}
})

app.use('/static', express.static('uploads'))

app.listen(port, function() {
    console.log('Starting node.js on port ' + port);
});


